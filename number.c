#include <stdio.h>
#define INIT 100
#define INIT2 2000

int countdown(int num)
{
	while(num)
	{
		printf("%d ", num);
		countdown(num-1);
	}
	printf("\n");

}

int main()
{
	countdown(INIT);
	countdown(INIT2);
}