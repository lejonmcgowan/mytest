typedef struct
{
	double x;
	double y;
	double z;

}Vector;

Vector addvector(Vector v1, Vector v2);
Vector subvector(Vector v1, Vector v2);
double dotproduct(Vector v1, Vector v2);
Vector scaleVector(double scale, Vector v);
Vector distance(Vector v);
Vector unitVector(Vector v);